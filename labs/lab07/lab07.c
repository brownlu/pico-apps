#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#define FLAG_VALUE 123

#define XIP_CTRL_BASE   (0x50110000u)

// PIO base address for XIP_CTRL
#define PIO_XIP_CTRL_BASE  (PIO0_BASE)

// Register offsets for XIP_CTRL PIO
#define PIO_XIP_CTRL_CTRL_REG   (*(volatile uint32_t *)(XIP_CTRL_BASE + 0x000))

// Define the bit position for XIP cache enable
#define XIP_CACHE_ENABLE_BIT    (1 << 0)
/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */

// Function to get the enable status of the XIP cache
bool get_xip_cache_en() {
    return (PIO_XIP_CTRL_CTRL_REG & XIP_CACHE_ENABLE_BIT) != 0;
}
// Function to set the enable status of the XIP cache
bool set_xip_cache_en(bool cache_en) {
    if (cache_en) {
        PIO_XIP_CTRL_CTRL_REG |= XIP_CACHE_ENABLE_BIT;
    } else {
        PIO_XIP_CTRL_CTRL_REG &= ~XIP_CACHE_ENABLE_BIT;
    }
    return get_xip_cache_en() == cache_en;
}


//Wallis Product function single precision
double WallisSingle(int x)
{
    //initialise variables
    float prod=1;
    float numerator =2;
    float firstDenominator =1;
    float secondDenominator=3;

for(int count =0; count <x; count++)
{
    float tempProd = (numerator/firstDenominator)*(numerator/secondDenominator); //calculate current product e.g. (2/1 * 2/3)
    //printf("Temp prod = %f\n",tempProd);

    //increment variables    
    firstDenominator = secondDenominator;
    secondDenominator+=2;
    numerator+=2;

    //multiply answer by calculated product
    prod *=tempProd;
    //printf("Prod = %f\n",prod);

}
return prod;
}

// Wallis Product function double precision
double WallisDouble(int x)
{
    //initialise variables
    double prod=1;
    double numerator =2;
    double firstDenominator =1;
    double secondDenominator=3;

for(int count =0; count <x; count++)
{
    double tempProd = (numerator/firstDenominator)*(numerator/secondDenominator); //calculate current product e.g. (2/1 * 2/3)
    //printf("Temp prod = %f\n",tempProd);

    //increment variables    
    firstDenominator = secondDenominator;
    secondDenominator+=2;
    numerator+=2;

    //multiply answer by calculated product
    prod *=tempProd;
    //printf("Prod = %f\n",prod);
}
return prod;
}

// Function to be run on the second core
void core1_entry() {
    while (1) {
        double (*func)(int) = (double (*)(int)) multicore_fifo_pop_blocking();
        int p = multicore_fifo_pop_blocking();
        double result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}
int main() {
    stdio_init_all();
     // Capture results for each scenario
    for (int i = 0; i < 4; i++) {
        bool both_cores = i / 2;
        bool cache_enabled = i % 2;
        if (set_xip_cache_en(cache_enabled)) {
            printf("XIP cache is %s\n", cache_enabled ? "enabled" : "disabled");
        } else {
            printf("Failed to set XIP cache status\n");
            return 1; // Exit with error code
        }


        // Measure execution times for single precision function
        uint64_t start_single = time_us_64();
        double res_single = WallisSingle(100000);
        uint64_t end_single = time_us_64();
        printf("Single Precision Runtime: %lu us\n", (uint32_t)(end_single - start_single));

        // Measure execution times for double precision function
        uint64_t start_double = time_us_64();
        double res_double = WallisDouble(100000);
        uint64_t end_double = time_us_64();
        printf("Double Precision Runtime: %lu us\n", (uint32_t)(end_double - start_double));

        // Measure total execution time of the application
        uint64_t start_total = time_us_64();
        multicore_launch_core1(core1_entry);
        multicore_fifo_push_blocking((uintptr_t)&WallisSingle);
        multicore_fifo_push_blocking(100000);
        multicore_fifo_push_blocking((uintptr_t)&WallisDouble);
        multicore_fifo_push_blocking(100000);
        double res_single_core1 = multicore_fifo_pop_blocking();
        double res_double_core1 = multicore_fifo_pop_blocking();
        uint64_t end_total = time_us_64();
        printf("Total Runtime: %lu us\n", (uint32_t)(end_total - start_total));
    }

    // Returning zero indicates everything went okay.
    return 0;
}