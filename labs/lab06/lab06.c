#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#define FLAG_VALUE 123
/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */
//Wallis Product function single precision
double WallisSingle(int x)
{
    //initialise variables
    float prod=1;
    float numerator =2;
    float firstDenominator =1;
    float secondDenominator=3;

for(int count =0; count <x; count++)
{
    float tempProd = (numerator/firstDenominator)*(numerator/secondDenominator); //calculate current product e.g. (2/1 * 2/3)
    //printf("Temp prod = %f\n",tempProd);

    //increment variables    
    firstDenominator = secondDenominator;
    secondDenominator+=2;
    numerator+=2;

    //multiply answer by calculated product
    prod *=tempProd;
    //printf("Prod = %f\n",prod);

}
return prod;
}

// Wallis Product function double precision
double WallisDouble(int x)
{
    //initialise variables
    double prod=1;
    double numerator =2;
    double firstDenominator =1;
    double secondDenominator=3;

for(int count =0; count <x; count++)
{
    double tempProd = (numerator/firstDenominator)*(numerator/secondDenominator); //calculate current product e.g. (2/1 * 2/3)
    //printf("Temp prod = %f\n",tempProd);

    //increment variables    
    firstDenominator = secondDenominator;
    secondDenominator+=2;
    numerator+=2;

    //multiply answer by calculated product
    prod *=tempProd;
    //printf("Prod = %f\n",prod);
}
return prod;
}

// Function to be run on the second core
void core1_entry() {
    while (1) {
        double (*func)(int) = (double (*)(int)) multicore_fifo_pop_blocking();
        int p = multicore_fifo_pop_blocking();
        double result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}
int main() {
    stdio_init_all();
    printf("Running Wallis Product approximation functions on multicore Pico!\n");

    uint64_t start_single, end_single, start_double, end_double, start_total, end_total;

    // Start total runtime measurement
    start_total = time_us_64();

    // Launch core 1 with the entry function
    multicore_launch_core1(core1_entry);

    // Start single precision runtime measurement
    start_single = time_us_64();

    // Push WallisSingle function pointer and test parameter to core 1
    multicore_fifo_push_blocking((uintptr_t) &WallisSingle);
    multicore_fifo_push_blocking(100000);

    // Pop the result from core 1
    double res_single = multicore_fifo_pop_blocking();

    // End single precision runtime measurement
    end_single = time_us_64();

    // Print single precision runtime
    printf("Single Precision Runtime: %lu us\n", (uint32_t)(end_single - start_single));

    // Start double precision runtime measurement
    start_double = time_us_64();

    // Push WallisDouble function pointer and test parameter to core 1
    multicore_fifo_push_blocking((uintptr_t) &WallisDouble);
    multicore_fifo_push_blocking(100000);

    // Pop the result from core 1
    double res_double = multicore_fifo_pop_blocking();

    // End double precision runtime measurement
    end_double = time_us_64();

    // Print double precision runtime
    printf("Double Precision Runtime: %lu us\n", (uint32_t)(end_double - start_double));

    // End total runtime measurement
    end_total = time_us_64();

    // Print total runtime
    printf("Total Runtime: %lu us\n", (uint32_t)(end_total - start_total));

    // Returning zero indicates everything went okay.
    return 0;
}