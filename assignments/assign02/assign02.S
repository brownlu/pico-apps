#include "hardware/regs/addressmap.h"
#include "hardware/regs/io_bank0.h"
#include "hardware/regs/timer.h"
#include "hardware/regs/m0plus.h"

.syntax unified
.cpu    cortex-m0plus
.thumb
.global main_asm
.align  4

.equ    DFLT_STATE_STRT,    1            @ Specify the value to start flashing
.equ    DFLT_STATE_STOP,    0            @ Specify the value to stop flashing
.equ    DFLT_ALARM_TIME,    1000000      @ Specify the default alarm timeout
.equ    SLEEP_TIME,         5000

.equ    GPIO_21_RISE_MSK,   0x00400000   @ Bit-22 for falling-edge event on GP21
.equ    GPIO_21_FALL_MSK,   0x00800000   @ Bit-23 for falling-edge event on GP21

.equ    GPIO_21,            21           @ Specify pin for the "enter" button
.equ    GPIO_LED_PIN,       28           @ Specify pin for the built-in LED
.equ    GPIO_DIR_IN,        0            @ Specify input direction for a GPIO pin
.equ    GPIO_DIR_OUT,       1            @ Specify output direction for a GPIO pin

.equ    GPIO_ISR_OFFSET,    0x74         @ GPIO is int #13 (vector table entry 29)
.equ    ALRM_ISR_OFFSET,    0x40         @ ALARM0 is int #0 (vector table entry 16)

.equ    DOT,                255

@@ ASM ENTRY POINT
main_asm:
    bl    init_buttons                   @ Initialise GPIO buttons
    //bl    init_led                     @ Initialise LED
    bl    install_button_isr             @ Install interrupt service routine (ISR) for buttons
    bl    install_timer_isr              @ Install interrupt service routine (ISR) for alarm interrupt
    
    ldr   r4, =DFLT_ALARM_TIME                      @ Store alarm delay in r4



restart:
    movs    r2, #0
    movs    r6, #1                                              @ Set currentQueation=true
    bl      display_level_choosing                              @ Display message for player to choose game
    bl      level_select_true                                   @ Set selecting_level=true
 
level_not_chosen:
    bl      level_input                                         @ get the level number  
    cmp     r6, #0
    bne     level_not_chosen                                    @ loop
    bl      level_select_false

Start:
    cmp     r6, #0                                              @ check if currentQuestion=true
    bne     solvedQues                                          @  
    movs    r6, #1                                              @ if not 
    bl      initalizeInputArray                                 @ Initialize array
    bl      get_level                                           @ Get current level                               
    cmp     r0, #2
    beq     if_level2
    cmp     r0, #1
    bl      level_1_question                                    @ Display level 1 question                         
    b       end_choosing
    
if_level2:
    bl      level_2_question                                    @ Display level 2 question
    b       end_choosing


end_choosing:
    movs    r4, #3                                              @ r4 shows whether a dot (0) or dash (1) has been pressed
    movs    r5, #0                                              @ r5 shows how many interrups/seconds have passed


solvedQues:
    bl      set_alarm                                           @ set  alarm
    wfi                                                         
    bl      user_input                                          
    cmp     r4, #3                                              @ check if  the button was pressed
    beq     notPressed                                          @  
    movs    r4, #0                                              @ reset r4

notPressed:                                                    @if button not pressed
    bl      check_level_complete                               @is the level over?                   
    cmp     r0, #1                                             @if level over
    beq     level_complete                                     @call C function
    cmp     r0, #2                                             @else
    beq     restart                                            @restart level
    b       Start                                              @Loop        


level_complete:
    bl      get_level                                          @Get current level
    cmp     r0, #1                                             @if 1
    beq     level1_complete	                                   @complete sequence for level 1
    cmp     r0, #2                                             @if 2
    beq     level2_complete                                    @complete sequence for level 2

level1_complete:
    
    bl     level_success                                       @Jump to C function           
    movs    r0, #2                                             @Parameter to set level as level 2
    bl      set_level                                          @Jump to C function
    b      finish_level                                        @Loop

level2_complete:
    
    bl     level_success                                       @Jump to C function
    b      finish_level                                        @Complete

finish_level:
    bl      startGame                                         @Restart game
    b       Start                                             @Loop
    
game_won:
    bl      game_won_display                                    @Jump to C function     
    b       restart                                             @New


game_over: 
    bl      game_over_display                                   @Jump to C function        
    b       restart                                             @New



@@ INITIALISATION FUNCTIONS
init_buttons:
    push    {lr}                        @ Push return address in link register to stack
    
    movs    r0, #GPIO_21                @ Store 'enter' pin in r0
    bl      asm_gpio_set_irq            @ Enable falling edge interrupts for pin
    movs    r0, #GPIO_21                @ Store 'enter' pin in r0
    movs    r1, #GPIO_DIR_IN            @ Store 'input' value in r1
    bl      asm_gpio_set_dir            @ Set direction of pin
    movs    r0, #GPIO_21                @ Store 'enter' pin in r0
    bl      asm_gpio_init               @ Initialise GPIO pin
    
    pop     {pc}                        @ Pop link register to program counter to return to original function



@@ INSTALLATION FUNCTIONS
install_button_isr:
    ldr     r2, =(PPB_BASE + M0PLUS_VTOR_OFFSET)        @ Load address of interrupt vector table (IVT) into r2
    ldr     r1, [r2]                                    @ Load IVT entry point address into r1
    movs    r2, #GPIO_ISR_OFFSET                        @ Load offset for GPIO ISR into r2
    add     r2, r1                                      @ Add values to find correct address
    ldr     r0, =button_isr                             @ Load button_isr into r0
    str     r0, [r2]                                    @ Store ISR in correct IVT address 
    ldr     r2, =(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)   @ Disable GPIO interrupt request (IRQ) by storing correct bit in NVIC Interrupt Clear Pending register
    ldr     r1, =0x2000                                 @ Disable GPIO interrupt request (IRQ) by storing correct bit in NVIC Interrupt Clear Pending register
    str     r1, [r2]                                    @ Disable GPIO interrupt request (IRQ) by storing correct bit in NVIC Interrupt Clear Pending register
    ldr     r2, =(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET)   @ Enable GPIO IRQ by storing correct bit in NVIC Interrupt Set Enable register
    ldr     r1, =0x2000                                 @ Enable GPIO IRQ by storing correct bit in NVIC Interrupt Set Enable register   
    str     r1, [r2]                                    @ Enable GPIO IRQ by storing correct bit in NVIC Interrupt Set Enable register
    bx      lr                                          @ Branch back to main_asm

install_timer_isr:
    ldr     r2, =(PPB_BASE + M0PLUS_VTOR_OFFSET)        @ Load address of interrupt vector table (IVT) into r2
    ldr     r1, [r2]                                    @ Load IVT entry point address into r1
    movs    r2, #ALRM_ISR_OFFSET                        @Load offset for alarm ISR into r2
    add     r2, r1                                      @ Add values to find correct address
    ldr     r0, =timer_isr                              @ Load timer_isr into r0
    str     r0, [r2]                                    @ Store ISR in correct IVT address
    
    ldr     r2, =(PPB_BASE + M0PLUS_NVIC_ICPR_OFFSET)   @ Disable alarm interrupt request (IRQ) by storing correct bit in NVIC Interrupt Clear Pending register
    movs    r1, #1                                      @ Disable alarm interrupt request (IRQ) by storing correct bit in NVIC Interrupt Clear Pending register
    str     r1 ,[r2]                                    @ Disable alarm interrupt request (IRQ) by storing correct bit in NVIC Interrupt Clear Pending register
    ldr     r2, =(PPB_BASE + M0PLUS_NVIC_ISER_OFFSET)   @ Enable alarm IRQ by storing correct bit in NVIC Interrupt Set Enable register
    movs    r1, #1                                      @ Enable alarm IRQ by storing correct bit in NVIC Interrupt Set Enable register
    str     r1 ,[r2]                                    @ Enable alarm IRQ by storing correct bit in NVIC Interrupt Set Enable register
    bx      lr                                          @ Branch back to main_asm

@@ TIMER FUNCTION
start_timer:
    ldr     r2, =(TIMER_BASE + TIMER_INTE_OFFSET)     @ Enable alarm interrupts
    movs    r1, #1                                    @ Enable alarm interrupts
    str     r1, [r2]                                  @ Enable alarm interrupts

    ldr     r1, =(TIMER_BASE + TIMER_TIMELR_OFFSET)  @ Load address of lower 32 bits of timer into r1
    ldr     r2, [r1]                                 @ Load timer address into r2
    movs    r1, r4                                   @ Store current delay in r1  
    add     r1, r1, r2                               @ Add delay to timer address
    ldr     r2, =(TIMER_BASE + TIMER_ALARM0_OFFSET)  @ Load alarm control address into r2
    str     r1, [r2]                                 @ Store timer with delay into control address
    bx      lr                                       @ Branch back to original function



@@ TIMER ISR FUNCTION
.thumb_func
timer_isr:
    push    {lr}                                    @ Push return address in link register to stack
    ldr     r1, =(TIMER_BASE+TIMER_INTR_OFFSET)     @ Load raw interrupts address into r1
    movs    r0 ,#1                                  @ Load 1 into r0
    str     r0,[r1]                                 @ Store 1 in raw interrupts address to acknowledge interrupt
    
    cmp     r7, #0                      @ If there is a time stored in r7, then the button is still being held
    bne     gpio_interrupt_in_progress
    cmp     r4, #3
    beq     gpio_interrupt_in_progress

    @ Add 1 to the number of iterations the timer has gone off in r5
    ldr     r3, =1
    add     r5, r5, r3

gpio_interrupt_in_progress:

    pop     {pc}                        @ Pop the link register from the stack to the program counter 


@@ GPIO BUTTON ISR FUNCTION
.thumb_func
button_isr:
    push {lr}                                                  @ Push return address in link register to stack
    
    ldr  r2, =(IO_BANK0_BASE + IO_BANK0_PROC0_INTS2_OFFSET)    @ Load GPIO interrupt status #2 address into r2
    Ldr  r1, [r2]   	                                       @ Load above address into r1
    ldr  r0 , =0x2000                                          @ Load value with bit 13 set into r0
    str  r0, [r2]                                              @ Store in interrupt address to acknowledge interrupt
    
    @ldr  r2, =(TIMER_BASE + TIMER_INTE_OFFSET)                 @ Load timer interrupt enable register into r2
    @ldr  r0, [r2]                                              @ Load above address into r2

    ldr  r5, =0 @ Maybe change reg number (r5 is time elapsed since interrupt)

    ldr  r2, =GPIO_21_RISE_MSK                                      @ Load GPIO 20 mask into r2 
    cmp  r2, r1                                                @ Compare with interrupt value
    beq  btn_21_pressed                                                  @ Branch to GP20 function

    ldr     r2, =GPIO_21_FALL_MSK
    cmp     r2, r1                                              @ Detect if it is a falling edge
    beq     btn_21_released
    b       end_input

btn_21_pressed:
    ldr     r2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)        @ Disable the pending interrupt from GPIO by writing the correct value to (IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)
    ldr     r1, =GPIO_21_RISE_MSK
    str     r1, [r2]

    bl      get_time_in_ms
    movs    r7, r0   @ Maybe change reg number (r7 is time of pressing here)                                          @ Store the start time in r7

    b       end_input

btn_21_released:
    ldr     r2, =(IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)        @ Disable the pending interrupt from GPIO by writing the correct value to (IO_BANK0_BASE + IO_BANK0_INTR2_OFFSET)
    ldr     r1, =GPIO_21_FALL_MSK
    str     r1, [r2]
    
    bl      get_time_in_ms
    movs    r1, r7     @ Maybe change reg number (r7 is time of pressing)                                          @ Load the start time
    bl      get_time_difference @ Calculating time difference between pressing and releasing (r0 and r1)
    movs    r7, r0
    bl      watchdog_update
    
    movs    r0, r7
    movs    r7, #0                                              @ Clear the time stored in r7
    cmp     r0, #DOT                                            @ If(time < 255ms)
    blt     input_dot                                                 @   input = dot
    bge     input_dash                                                @ else
    b       end_input                                           @   input = dash

input_dot:
    ldr     r4, =1                                              @ 1 = "."
    b       end_input

input_dash:
    ldr     r4, =2                                              @ 2 = "-"

end_input:

    pop     {pc}                                                @ Exit ISR   

@ Set data alignment
.data
    .align 4
